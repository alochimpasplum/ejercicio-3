******
README
******

**AUTOR**: C.A. Juan Antonio Lagos Carrera

**FECHA DE ENTREGA**: 14/02/14

INDICE
######

#. `Elementos de la web`_
#. `Lenguaje de marcado de la web`_
#. `Problemas surgidos`_
#. `Etiquetas HTML`_
#. `Etiquetas CSS`_


ELEMENTOS DE LA WEB
###################

La web consta de 10 p�ginas en HTML, de las cuales el navegador en cuanto accede al lugar donde est�n alojadas abre el archivo llamado **index.html**.

As�mismo hay dos carpetas, una en la que alojo las imagenes de la web y otro en la que alojo el archivo mp3 con el conxuro de la queimada.

LENGUAJE DE MARCADO DE LA WEB
#############################

He escogido el lenguaje de marcado HTML porque pese a que no se ha explicado en exceso en clase, considero que es lo suficientemente sencillo, y que ademas permite mas funciones que restructuredtext para hacer una buena presentacion.

Para insertar el fondo de la web, he usado CSS, que da una mayor flexibilidad a HTML ya que permite mejor la presentaci�n.

PROBLEMAS SURGIDOS
##################

Me han surgido dos problemas cuando estaba dise�ando la web, y es que HTML de por si, no permite ni las vocales acentuadas, ni la e�e.

ETIQUETAS HTML
##############

* <!DOCTYPE html> Indica al navegador en que tipo de lenguaje de marcas esta escrito el documento para mejorar la lectura y muestra del mismo.
* <html> Indica que dentro de los corchetes el lenguaje de marcas usado es en html
* <style> Inidica que dentro de las etiquetas est� escrito el lenguaje CSS detallado en el siguiente apartado.
* <head> Es la cabecera del documento.
* <body> El cuerpo que alberga todo el documento.
* <h1><h2> Son tipos de t�tulos.
* <center> Centra el texto o elemento.
* <p> Delimita p�rrafos
* <b> Pone el texto en negrita (bold)
* <img src="ubicacion de la imagen" width="anchura en porcentaje de la pagina" height="altura en procentaje de la pagina"> Inserta una imagen con los par�metros establecidos.
* <table> Establece una tabla.
* <thead> Es la cabecera de la tabla
* <tbody> Es el cuerpo de la tabla.
* <tr> Establece una fila de la tabla.
* <td> seg�n se va introduciendo dentro de la fila, va delimitando las celdas.
* <ol> Establece una lista ordenada.
* <ul> Establece una lista no ordenada.
* <li> Va introduciendo l�neas dentro de la lista.
* <a href="enlace a un elemento multimedia" target="indica en donde se abrira este enlace">
* <a href=URL>Nombre a mostrar</a> Inserta un hiperv�nculo enlazado desde el nombre a mostrar.

ETIQUETAS CSS
#############

* body { elementos a modificar ;} Todos los atributos cambiados dentro de los corchetes afectan a todo el documento.
* background-image:url("ubicacion imagen"); Establece una imagen de fodo indicando su ubicaci�n.
* background-attackment: fixed; Indica que la imagen de fondo se quedar� est�tica cuando bajemos o subamos la pagina.
* background-size: cover; Establece que la imagen de fonde se ajustara a la pantalla independientemente de la resoluci�n del navegador.